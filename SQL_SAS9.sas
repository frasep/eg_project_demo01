/*************************/
/* Requete SQL sur SAS 9 */
/*************************/

proc sql;
	create table locdata.myagg as
	select Facility, Unit, sum(Revenue) as sum_r, sum(Profit) as sum_p from locdata.megacorp5_4m 
		group by Facility, Unit;
quit;
