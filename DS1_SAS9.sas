/* Label: Data step SAS 9.x */

proc sort data=locdata.megacorp5_4m out=locdata.megacorp5_4m_s;
by DateByMonth UnitStatus;
run;

data locdata.ds_output_megacorp5_4m;
	length unit_status_level $ 8;
	set locdata.megacorp5_4m_s;
	by DateByMonth UnitStatus;
  	retain cumulative_actual;
  	if first.DateByMonth then cumulative_actual = actual;
 	else cumulative_actual = cumulative_actual + actual;

	/* Transform Value */
	select (UnitStatus);
		when ('active')  unit_status_level='A';
		when ('closed')  unit_status_level='C';
		when ('upkeep')  unit_status_level='U';
		when ('failure')  unit_status_level='F';
		when ('upgrade')  unit_status_level='G';
		otherwise unit_status_level='Unknown';
	end;

	if last.DateByMonth and last.UnitStatus then output;

	if done then
		put "THREAD :" _threadid_ "NUMBER OF RECORDS :" _N_;
run;