/* Label: Proc summary sur CAS */
proc summary data=mydata.megacorp5_4m print;
	by facility productline;
	var Expenses Revenue FacilityAge ;
	ods output Summary=casuser.proc_summary;
run;