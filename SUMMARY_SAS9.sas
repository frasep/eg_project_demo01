
/* Label: Proc summary sur SAS 9 */
proc sort data=locdata.megacorp5_4m;
by facility productline;
run;

proc summary data=locdata.megacorp5_4m print;
	by facility productline;
	var Expenses Revenue FacilityAge;
	ods output Summary=proc_summary;
run;