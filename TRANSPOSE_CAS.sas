

proc fedsql  sessref=mysess;
	create table casuser.megaagg as 
	select 
		facilityregion, 
		facility, 
		product, 
		unit, 
		avg(unitage) as unitage, 
		avg(unitreliability) as unitreliability, 
		avg(revenue) as revenue 
	from 
		mydata.megacorp5_4m 
	group by 
		facilityregion, facility, product, unit;
	quit;

proc transpose data=casuser.megaagg out=casuser.trans_megacorp;
	by facilityregion facility product;
	var unitreliability;
	id unit;
run;